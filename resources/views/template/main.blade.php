<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>Droptienda - Documentation and Developer Guide</title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="description" content="Droptienda documentation and developer quick guide">
    <meta name="author" content="Zunaid Miah">    
    <link rel="shortcut icon" href="{{ asset('logo/logo.png') }}"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="{{ asset('assets/fontawesome/js/all.min.js') }}"></script>
    
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.2/styles/atom-one-dark.min.css">
    
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.2/styles/atom-one-dark.min.css">

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="{{ asset('assets/css/theme.css') }}">
    
    <style>
        div#preloader {
            background: #000 url("{{ asset('images/preloader.gif') }}") no-repeat scroll center center;
            height: 100%;
            background-color: white;
            left: 0;
            overflow: visible;
            position: fixed;
            top: 0;
            width: 100%;
            z-index: 9999;
        }
    </style>

</head> 

<body class="docs-page">    
<div id="preloader"></div>
    <header class="header fixed-top">	    
        <div class="branding docs-branding">
            <div class="container-fluid position-relative py-2">
                <div class="docs-logo-wrapper">
					<button id="docs-sidebar-toggler" class="docs-sidebar-toggler docs-sidebar-visible mr-2 d-xl-none" type="button">
	                    <span></span>
	                    <span></span>
	                    <span></span>
	                </button>
	                <div class="site-logo"><a class="navbar-brand" href="/"><span class="logo-text">Droptienda<span class="text-alt">Docs</span></span></a></div>    
                </div><!--//docs-logo-wrapper-->
	            <div class="docs-top-utilities d-flex justify-content-end align-items-center">
	                <div class="top-search-box d-none d-lg-flex">
		                <form class="search-form">
				            <input type="text" placeholder="Search the docs..." name="search" class="form-control search-input">
				            <button type="submit" class="btn search-btn" value="Search"><i class="fas fa-search"></i></button>
				        </form>
	                </div>
	
					<ul class="social-list list-inline mx-md-3 mx-lg-5 mb-0 d-none d-lg-flex">
						<li class="list-inline-item"><a href="https://bitbucket.org/shojol80/droptienda_latest_ver/src/master/"><i class="fab fa-github fa-fw"></i></a></li>
			            <!-- <li class="list-inline-item"><a href="#"><i class="fab fa-twitter fa-fw"></i></a></li> -->
		                <li class="list-inline-item"><a href="https://bamboo.droptienda.rocks"><i class="fab fa-slack fa-fw"></i></a></li>
		                <!-- <li class="list-inline-item"><a href="#"><i class="fab fa-product-hunt fa-fw"></i></a></li> -->
		            </ul><!--//social-list-->
		            <a href="https://bitbucket.org/shojol80/droptienda_latest_ver/src/master/" class="btn btn-primary d-none d-lg-flex">Download</a>
	            </div><!--//docs-top-utilities-->
            </div><!--//container-->
        </div><!--//branding-->
    </header><!--//header-->

    <div id="docs-sidebar" class="docs-sidebar">
    <div class="container">
        <nav id="docs-nav" class="docs-nav navbar">
            <ul class="section-items list-unstyled nav flex-column pb-3">
                <li class="nav-item section-title"><a class="" href="/introduction"><span class="theme-icon-holder mr-2"><i class="fas fa-map-signs"></i></span>Introduction</a></li>
                <?php  
                    use Illuminate\Support\Facades\DB;
                    $id = DB::table('category')->where(['category'=>'introduction'])->first();
                    $data = DB::table('sub_category')->where(['cat_id'=>$id->id])->get();
                ?>
                @foreach($data as $item)
                    <li class="nav-item"><a class="nav-link" href="/introduction#<?php echo $item->sub_category; ?>">{{   $item->sub_category  }}</a></li>
                @endforeach
                <li class="nav-item section-title mt-3"><a class="" href="/installation"><span class="theme-icon-holder mr-2"><i class="fas fa-arrow-down"></i></span>Installation</a></li>
                <?php
                    $id = DB::table('category')->where(['category'=>'installation'])->first();
                    $data = DB::table('sub_category')->where(['cat_id'=>$id->id])->get();
                    // dd($data);
                ?>
                @foreach($data as $item)
                    <li class="nav-item"><a class="nav-link" href="/installation#<?php echo $item->sub_category; ?>">{{   $item->sub_category  }}</a></li>
                @endforeach

                <li class="nav-item section-title mt-3"><a class="" href="/templates"><span class="theme-icon-holder mr-2"><i class="fas fa-book-reader"></i></span>Templates</a></li>

                <?php
                    $data = DB::table('template_info')->get();
                    // dd($data);
                ?>
                @foreach($data as $item)
                    <li class="nav-item"><a class="nav-link" href="/templates#<?php echo $item->title; ?>">{{   $item->title  }}</a></li>
                @endforeach

                <li class="nav-item section-title mt-3"><a class="" href="/features"><span class="theme-icon-holder mr-2"><i class="fas fa-laptop-code"></i></span>Features</a></li>

                <?php
                    $id = DB::table('category')->where(['category'=>'Features'])->first();
                    $data = DB::table('sub_category')->where(['cat_id'=>$id->id])->get();
                    // dd($data);
                ?>
                @foreach($data as $item)
                    <li class="nav-item"><a class="nav-link" href="/features#<?php echo $item->sub_category; ?>">{{   $item->sub_category  }}</a></li>
                @endforeach

                <!-- <li class="nav-item section-title mt-3"><a class="" href=""><span class="theme-icon-holder mr-2"><i class="fas fa-box"></i></span>APIs</a></li>
                
                <li class="nav-item section-title mt-3"><a class="" href=""><span class="theme-icon-holder mr-2"><i class="fas fa-tools"></i></span>Modules</a></li>
                
                <li class="nav-item section-title mt-3"><a class="" href=""><span class="theme-icon-holder mr-2"><i class="fas fa-tablet-alt"></i></span>Resources</a></li> -->
                
                <li class="nav-item section-title mt-3"><a class="" href="/faqs"><span class="theme-icon-holder mr-2"><i class="fas fa-lightbulb"></i></span>FAQs</a></li>
                
            </ul>

        </nav><!--//docs-nav-->
    </div><!--//docs-sidebar-->
</div>
<div class="docs-content">
    @yield('body')
</div>
<footer class="footer">
        <div class="container text-center py-5">
            <!--/* This template is free as long as you keep the footer attribution link. If you'd like to use the template without the attribution link, you can buy the commercial license via our website: themes.3rdwavemedia.com Thank you for your support. :) */-->
            <small class="copyright">© Copyright 2021 Droptienda CMS Ltd.</small>
            <ul class="social-list list-unstyled pt-4 mb-0">
                <li class="list-inline-item"><a href="https://bitbucket.org/shojol80/droptienda_latest_ver/src/master/"><i class="fab fa-github fa-fw"></i></a></li> 
                <li class="list-inline-item"><a href="#"><i class="fab fa-twitter fa-fw"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fab fa-slack fa-fw"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fab fa-product-hunt fa-fw"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f fa-fw"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fab fa-instagram fa-fw"></i></a></li>
            </ul><!--//social-list-->
        </div>
    </footer>
   
       
    <!-- Javascript -->          
    <script src="{{ asset('assets/plugins/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/popper.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>  
    
    
    <!-- Page Specific JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.8/highlight.min.js"></script>
    <script src="{{ asset('assets/js/highlight-custom.js') }}"></script> 
    <script src="{{ asset('assets/plugins/jquery.scrollTo.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/lightbox/dist/ekko-lightbox.min.js') }}"></script> 
    <script src="{{ asset('assets/js/docs.js') }}"></script> 
    <script>
        $(window).on('load', function() {
            $('#preloader').fadeOut('slow', function() { $(this).remove(); });
        });
    </script>
</body>
</html> 