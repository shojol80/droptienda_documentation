<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use GuzzleHttp\Client;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function installContent() {
        return view('contentInstallation');
    }


    public function introContent() {
        return view('contentIntro');
    }


    public function templateContent() {
        
        $client = new Client();
        $res = $client->request('GET', 'https://packages.droptienda-templates.com/api/drm_template_details');
        // echo $res->getStatusCode();
        // "200"
        // var_export ($res->getBody());
        // 'application/json; charset=utf8'
        $data = json_decode($res->getBody(), true);
        $data = $data['data'];
        // dd($data);
        return view('contentTemplate', compact('data'));
    }

    public function featuresContent() {
        return view('contentFeatures');
    }

    public function faq() {
        return view('contentFaq');
    }


}
