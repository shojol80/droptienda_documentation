<?php

namespace App\Http\Controllers;
use App\Models\faq_info;
use App\Models\content;
use App\Models\template_info;
use App\Models\introductions_content;
use App\Models\installation_content;
use App\Models\sub_category;
use Illuminate\Http\Request;
use DB;

class AdminController extends Controller
{
    public function adminAuthentication(REQUEST $request)
    {
        $admin = $request['admin'];
        $password = $request['password'];
        if ($admin == "admin" && $password == "admin1234") {
            $request->session()->put('adminUsername',"admin");
            return redirect('/admin');
        }
        else{
            return redirect()->back();
        }
    }

    public function dashboard()
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            return view('admin.index');
        }
    }

    public function logout()
    {
        session()->put('adminUsername', null);
        return redirect('/adminPanel');
    }

    public function addFaq()
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            return view('admin.addFaq');
        }
    }

    public function addFaqInfo(REQUEST $request)
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            $faq = new faq_info();
            $faq->question = $request['question'];
            $faq->answer = $request['answer'];
            $faq->save();
            return redirect('/addFaq');
        }
    }

    public function deleteFaq($id)
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            DB::table('faq_infos')->where('id',$id)->delete();
            return redirect('/addFaq');
        }
    }

    public function addFeature()
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            return view('admin.addFeature');
        }
    }

    public function addFeatureData(REQUEST $request)
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            $con = new content();
            $con->sub_cat_id = $request['id'];
            $con->type = "category";
            $con->value = $request['feature'];
            $con->save();
            return redirect('/addFeature');
        }
    }


    public function deleteFeature($id)
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            DB::table('content')->where('id',$id)->delete();
            return redirect('/addFeature');
        }
    }

    public function addTemplate()
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            return view('admin.addTemplate');
        }
    }

    public function addTemplateInfo(REQUEST $request)
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            $tem = new template_info();
            $tem->title = $request['title'];
            $tem->link = $request['link'];
            $tem->save();
            return redirect('/addTemplate');
        }
    }


    public function deleteTemplate($id)
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            DB::table('template_info')->where('id',$id)->delete();
            return redirect('/addTemplate');
        }
    }


    public function addIntro()
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            return view('admin.addIntro');
        }
    }
    

    public function addIntroInfo(REQUEST $request)
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            $content = new introductions_content();
            $content->sub_cat_id = $request['id'];
            $content->content = $request['content'];
            $content->save();
            return redirect()->back();
            // return $request['content'];
        }
    }

    public function updateIntroInfo(REQUEST $request)
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            // dd($request['id']);
            DB::table('introductions_contents')
                ->where('id', $request['id'])
                ->update(['content' => $request['content']]);
            return redirect('/addIntro');
        }
    }

    public function deleteIntroInfo($id)
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            // dd($request['id']);
            DB::table('introductions_contents')
                ->where('id', $id)
                ->delete();
            return redirect('/addIntro');
        }
    }


    public function addInstall()
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            return view('admin.addInstall');
        }
    }


    public function addInstallInfo(REQUEST $request)
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            $content = new installation_content();
            $content->sub_cat_id = $request['id'];
            $content->content = $request['content'];
            $content->save();
            return redirect()->back();
            // return $request;
        }
    }


    public function updateInstallInfo(REQUEST $request)
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            // dd($request['id']);
            DB::table('installation_contents')
                ->where('id', $request['id'])
                ->update(['content' => $request['content']]);
            return redirect('/addInstall');
        }
    }  
    
    
    public function deleteInstallationInfo($id)
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            // dd($request['id']);
            DB::table('installation_contents')
                ->where('id', $id)
                ->delete();
            return redirect('/addInstall');
        }
    }

    public function addSubCategory()
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            return view('admin.addSubCategory');
            // $cat = new sub_category();
            // $cat->cat_id = $request['id'];
            // $cat->sub_category = $request['id'];
            // $car->save();
            // return redirect('/addSubCategory');
        }
    }
    public function addSubCategoryData(REQUEST $request)
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            $cat = new sub_category();
            $cat->cat_id = $request['id'];
            $cat->sub_category = $request['subCategory'];
            $cat->save();
            return redirect('/addSubCategory');
        }
    }

    public function deleteSubCategory($id)
    {
        if(session('adminUsername')==null)
        {
            return redirect('/adminPanel');
        }
        else
        {
            // dd($request['id']);
            DB::table('sub_category')
                ->where('id', $id)
                ->delete();
            return redirect('/addSubCategory');
        }
    }



}
