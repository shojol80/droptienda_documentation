<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class installation_content extends Model
{
    use HasFactory;
    protected $table ='installation_contents';
    protected $fillable = ['sub_cat_id', 'content'];

}
