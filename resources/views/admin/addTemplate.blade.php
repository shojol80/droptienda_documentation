@extends('admin/template/admindashboard')

@section('body')
<div class="row">
    <ol class="breadcrumb">
        <li class="active">Add Template Information</li>
    </ol>
</div><!--/.row-->  

    <!-- /Page Header -->
    <br><br>
        <form method="post" action="/addTemplateInfo">
            @csrf
            <div class="row mb-3">
                <div class="col-sm-4">
                <input type="text" class="form-control" name="title" placeholder="Enter template title">
                </div>
                <div class="col-sm-4">
                <input type="text" class="form-control" name="link" placeholder="Enter the answer here">
                </div>
                <div class="col-sm-4">
                    <input type="submit" class="btn btn-primary add" value="Add Info">

                </div>
            </div>
        </form>
        <br><br>

    <?php
        use Illuminate\Support\Facades\DB;
        $data=DB::table('template_info')->get()->all();


    ?>
 <div class="row">
        <div class="col-sm-12">
        
            <div class="card card-table">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-center mb-0 datatable">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Link</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($data)):  ?>
                                    <?php foreach($data as $data):  ?>
                                    <tr>
                                        <td><?php print $data->title;  ?></td>
                                        <td><?php print $data->link;  ?></td>
                                        <td>
                                            <div class="actions">
                                                <a href="/deleteTemplate/{{ $data->id }}" class="btn btn-sm btn-danger">
                                                    Delete
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>                          
        </div>                  
    </div>

@endsection
