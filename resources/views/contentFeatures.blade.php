
@extends('template/main')


@section('body')

<section class="docs-section ml-4 mt-5" id="item-1-1">
<?php
    $id = DB::table('category')->where('category', 'features')->first();
    $data = DB::table('sub_category')->where('cat_id', $id->id)->get();
    // dump($data);
    foreach($data as $item) : ?>
        <div id="<?php echo $item->sub_category; ?>" class="mt-5">
            <ol>   
                <b><?php echo $item->sub_category; ?></b>
                <?php $sub_id = DB::table('content')->where(['sub_cat_id'=>$item->id])->get();
                // dump($sub_id);
                foreach($sub_id as $sub_item) : ?>
                    <li class="ml-5"><?php echo  $sub_item->value; ?></li>
                <?php endforeach; ?>
            </ol>
         </div>
         <hr>
    <?php endforeach; ?>
</section><!--//section-->
@endsection
