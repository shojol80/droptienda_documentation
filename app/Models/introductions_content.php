<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class introductions_content extends Model
{
    use HasFactory;
    protected $table ='introductions_contents';
    protected $fillable = ['sub_cat_id', 'content'];
}
