@extends('admin/template/admindashboard')

@section('body')

<div class="row">
    <ol class="breadcrumb">
        <li class="active">Add Sub Category</li>
    </ol>
</div><!--/.row-->  
<br><br>

<?php
        $data=DB::table('category')->get();

    ?>
    <div class="container">
        <form action="/addSubCategoryData" method="post">
            @csrf
            <div class="row">
                <div class="col-sm-4">
                    <label>Select Category</label>
                    <select class="form-control" name="id">
                        @foreach($data as $item)
                            <option value="{{ $item->id }}">{{ $item->category }}</option>
                        @endforeach
                    
                    </select>
                </div>
                <div class="col-sm-4">
                    <label>Add Sub Category</label>
                    <input type="text" class="form-control" name="subCategory" placeholder="add Sub Category">
                </div>
                <div class="col-sm-4">
                    <input type="submit" class="btn btn-primary" value="Add Sub Category">

                </div>
            </div>

        </form>
    </div>
    <br><br>
    <div class="row">
        <div class="col-sm-12">
        
            <div class="card card-table">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-center mb-0 datatable">
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Sub Category</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($data)):  ?>
                                    <?php foreach($data as $data):  ?>
                                        <?php $sub_id = DB::table('sub_category')->where(['cat_id'=>$data->id])->get();
                                        // dump($sub_id);
                                        foreach($sub_id as $sub_item) : ?>
                                            <tr>
                                                <td><?php print $data->category;  ?></td>
                                                <td><?php print $sub_item->sub_category;  ?></td>
                                                <td>
                                                    <div class="actions">
                                                        <a href="/deleteSubCategory/{{ $sub_item->id }}" class="btn btn-sm btn-danger">
                                                            Delete
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    
                                    <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>                          
        </div>                  
    </div>
   

@endsection