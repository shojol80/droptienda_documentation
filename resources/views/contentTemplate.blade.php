
@extends('template/main')

@section('body')
<section class="docs-section ml-4 mt-4" id="item-1-1">
    <br><br>
    <?php  foreach ($data as $item) : ?>
        <div id="<?php echo $item['name'];  ?>">
            <h4>Template: <?php echo $item['name'];  ?></h4><br>
            <?php $counterImg = count($item['images']); ?>
            <p><b>Template Demo: </b></p>
            <?php for ($i=0; $i < $counterImg; $i++) : ?>
                <a href="<?php echo $item['images'][$i]; ?>" target="_blank"><img src="<?php echo $item['images'][$i]; ?>" alt="" height="450px" width="45%" style="margin :3px 3px;border:.5px solid black;"></a>
            <?php endfor; ?>
            <p class="mt-5"><b>Description: </b><?php echo $item['description'];  ?></p>
            <?php $counter = count($item['versions']); ?>
            <p><b>Available Versions: &nbsp;</b>
                <?php for ($i=0; $i < $counter; $i++) : ?>
                    <?php echo $item['versions'][$i]; ?> 
                    <?php if (($i+1)<$counter) : ?>
                    &nbsp;>&nbsp;
                    <?php endif; ?>
                <?php endfor; ?>
            </p><br>
            <?php 
                $link = "";
                $templateInfo = DB::table('template_info')->get();
                foreach ($templateInfo as $info) {
                    if ($item['name'] == $info->title) {
                        $link = $info->link;
                    }
                }
            ?>
            <a href="<?php echo $link; ?>" class="btn btn-primary" target="_blank">Live Preview</a>
            <br><br><hr><br>
        </div>
    <?php endforeach; ?>
    

</section><!--//section-->


@endsection
