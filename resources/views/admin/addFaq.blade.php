@extends('admin/template/admindashboard')

@section('body')
<div class="row">
    <ol class="breadcrumb">
        <li class="active">Frequently asked questions</li>
    </ol>
</div><!--/.row-->  

    <!-- /Page Header -->
    <br><br>
        <form method="post" action="/addFaqInfo">
            @csrf
            <div class="row mb-3">
                <div class="col-sm-4">
                <textarea class="form-control" name="question" rows="3" placeholder="Enter the question here"></textarea>
                </div>
                <div class="col-sm-4">
                <textarea class="form-control" name="answer" rows="3" placeholder="Enter the answer here"></textarea>
                </div>
                <div class="col-sm-4">
                    <input type="submit" class="btn btn-primary add" value="Add">

                </div>
            </div>
        </form>
        <br><br>

    <?php
        use Illuminate\Support\Facades\DB;
        $data=DB::table('faq_infos')->get()->all();


    ?>
 <div class="row">
        <div class="col-sm-12">
        
            <div class="card card-table">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-center mb-0 datatable">
                            <thead>
                                <tr>
                                    <th>Questions</th>
                                    <th>Answers</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($data)):  ?>
                                    <?php foreach($data as $data):  ?>
                                    <tr>
                                        <td><?php print $data->question;  ?></td>
                                        <td><?php print $data->answer;  ?></td>
                                        <td>
                                            <div class="actions">
                                                <a href="/deleteFaq/{{ $data->id }}" class="btn btn-sm btn-danger">
                                                    Delete
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>                          
        </div>                  
    </div>

@endsection
