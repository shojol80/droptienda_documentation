<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class faq_info extends Model
{
    use HasFactory;
    protected $table ='faq_infos';
    protected $fillable = ['question', 'answer'];
}
