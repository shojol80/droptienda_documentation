<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class template_info extends Model
{
    use HasFactory;
    protected $table ='template_info';
    protected $fillable = ['title', 'link'];
}
