@extends('admin/template/admindashboard')

@section('body')

<div class="row">
    <ol class="breadcrumb">
        <li class="active">Add Template Features</li>
    </ol>
</div><!--/.row-->  
<br><br>

<?php
        $data=DB::table('category')->where('category', "features")->first();
        // dump ($data->id);
        $sub_cat = DB::table('sub_category')->where('cat_id', $data->id)->get();
        // dump ($sub_cat);

    ?>
    <div class="container">
        <form action="/addFeatureData" method="post">
            @csrf
            <div class="row">
                <div class="col-sm-4">
                    <label>Select Sub category</label>
                    <select class="form-control" name="id">
                        @foreach($sub_cat as $item)
                            <option value="{{ $item->id }}">{{ $item->sub_category }}</option>
                        @endforeach
                    
                    </select>
                </div>
                <div class="col-sm-4">
                    <label>Add Feature</label>
                    <input type="text" class="form-control" name="feature" placeholder="add feature here">
                </div>
                <div class="col-sm-4">
                    <input type="submit" class="btn btn-primary" value="Add Featured">

                </div>
            </div>

        </form>
    </div>

    <br><br>
    <div class="row">
        <div class="col-sm-12">
        
            <div class="card card-table">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-center mb-0 datatable">
                            <thead>
                                <tr>
                                    <th>Sub Category</th>
                                    <th>Feature</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($sub_cat)):  ?>
                                    <?php foreach($sub_cat as $data):  ?>
                                        <?php $sub_id = DB::table('content')->where(['sub_cat_id'=>$data->id])->get();
                                        // dump($sub_id);
                                        foreach($sub_id as $sub_item) : ?>
                                            <tr>
                                                <td><?php print $data->sub_category;  ?></td>
                                                <td><?php print $sub_item->value;  ?></td>
                                                <td>
                                                    <div class="actions">
                                                        <a href="/deleteFeature/{{ $sub_item->id }}" class="btn btn-sm btn-danger">
                                                            Delete
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    
                                    <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>                          
        </div>                  
    </div>


@endsection