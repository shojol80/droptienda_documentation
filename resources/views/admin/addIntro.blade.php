@extends('admin/template/admindashboard')

@section('body')

<div class="row">
    <ol class="breadcrumb">
        <li class="active">Add Introduction Content</li>
    </ol>
</div><!--/.row-->  
<br><br>
<?php
    $data=DB::table('category')->where('category', "introduction")->first();
    // dump ($data->id);
    $sub_cat = DB::table('sub_category')->where('cat_id', $data->id)->get();
    // dump ($sub_cat);

?>

<?php if(isset($_GET['edit'])) : ?>
    <?php
        $edit_content = DB::table('introductions_contents')->where('id', $_GET['edit'])->first();
        // dd($edit_content->id);
    ?>
    <div id="update">
        <form action="/updateIntroInfo" method="post">
            @csrf
            <label>Update Content</label>
            <input type="hidden" name="id" value="<?php print $edit_content->id ?>">
            <textarea class="ckeditor" cols="80" id="content" name="content" rows="10"><?php print $edit_content->content; ?></textarea><br>
            <input type="submit" class="btn btn-primary" value="Update">
        </form>
    </div>
<?php else : ?>
    <div id="add">
        <form action="/addIntroInfo" method="post">
            @csrf
            <div class="row">
                <div class="col-sm-6">
                    <label>Select Sub category</label>
                    <select class="form-control" name="id">
                        @foreach($sub_cat as $item)
                            <option value="{{ $item->id }}">{{ $item->sub_category }}</option>
                        @endforeach
                    
                    </select>
                </div>
            </div>
            <br><br>
            <label>Add Content here</label>
            <textarea class="ckeditor" cols="80" id="content" name="content" rows="10"> </textarea><br>
            <input type="submit" class="btn btn-primary" value="Add">
        </form>
    </div>
<?php endif; ?>

<div style="margin:10px 10px;">
    <?php
        $id = DB::table('category')->where('category', 'introduction')->first();
        $data = DB::table('sub_category')->where('cat_id', $id->id)->get();
        // dump($data);
        foreach($data as $item) : ?>
            <div class="mt-5">
                <h2><?php echo $item->sub_category; ?></h2>
                <?php $sub_id = DB::table('introductions_contents')->where(['sub_cat_id'=>$item->id])->get();
                // dump($sub_id);
                foreach($sub_id as $sub_item) : ?>
                    <?php echo  $sub_item->content; ?><br>
                    <a href="?edit=<?php print $sub_item->id; ?>" class="btn btn-warning">Edit</a>
                    <a href="delete/<?php print $sub_item->id; ?>" class="btn btn-danger" style="margin-left: 10px;">Delete</a>
                <?php endforeach; ?>
            </div>
            <hr>
        <?php endforeach; ?>

</div>


@endsection