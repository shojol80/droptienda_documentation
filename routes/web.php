<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/installation', [Controller:: class, 'installContent']);

Route::get('/templates', [Controller:: class, 'templateContent']);

Route::get('/introduction', [Controller:: class, 'introContent']);

Route::get('/features', [Controller:: class, 'featuresContent']);

Route::get('/faqs', [Controller:: class, 'faq']);

Route::get('/apis', function () {
    return view('error.404');
});

Route::get('/function', function () {
    return view('error.404');
});

Route::get('/modules', function () {
    return view('error.404');
});

Route::get('/resources', function () {
    return view('error.404');
});



//Admin Part
Route::get('/adminPanel', function () {
    return view('admin/login');
});

Route::any('/adminDashboard', [AdminController:: class, 'adminAuthentication']);

Route::get('/admin', [AdminController:: class, 'dashboard']);

Route::get('/logout', [AdminController:: class, 'logout']);

Route::get('/addFaq', [AdminController:: class, 'addFaq']);

Route::post('/addFaqInfo', [AdminController:: class, 'addFaqInfo']);

Route::get('/deleteFaq/{id}', [AdminController:: class, 'deleteFaq']);

Route::get('/addFeature', [AdminController:: class, 'addFeature']);

Route::post('/addFeatureData', [AdminController:: class, 'addFeatureData']);

Route::get('/deleteFeature/{id}', [AdminController:: class, 'deleteFeature']);

Route::get('/addTemplate', [AdminController:: class, 'addTemplate']);

Route::post('/addTemplateInfo', [AdminController:: class, 'addTemplateInfo']);

Route::get('/deleteTemplate/{id}', [AdminController:: class, 'deleteTemplate']);

Route::get('/addIntro', [AdminController:: class, 'addIntro']);

Route::post('/addIntroInfo', [AdminController:: class, 'addIntroInfo']);

Route::post('/updateIntroInfo', [AdminController:: class, 'updateIntroInfo']);

Route::get('/delete/{id}', [AdminController:: class, 'deleteIntroInfo']);

Route::get('/addInstall', [AdminController:: class, 'addInstall']);

Route::post('/addInstallInfo', [AdminController:: class, 'addInstallInfo']);

Route::post('/updateInstallInfo', [AdminController:: class, 'updateInstallInfo']);

Route::get('/deleteInstallation/{id}', [AdminController:: class, 'deleteInstallationInfo']);

Route::get('/addSubCategory', [AdminController:: class, 'addSubCategory']);

Route::post('/addSubCategoryData', [AdminController:: class, 'addSubCategoryData']);

Route::get('/deleteSubCategory/{id}', [AdminController:: class, 'deleteSubCategory']);
