
@extends('template/main')


@section('body')

<section class="docs-section ml-4 mt-4">
    <div id="Droptienda Installation">
    <div style="margin:10px 10px;">
    <?php
        $id = DB::table('category')->where('category', 'installation')->first();
        $data = DB::table('sub_category')->where('cat_id', $id->id)->get();
        // dump($data);
        foreach($data as $item) : ?>
            <div class="mt-5" id="<?php echo $item->sub_category; ?>">
                <h2><?php echo $item->sub_category; ?></h2><hr><br>
                <?php $sub_id = DB::table('installation_contents')->where(['sub_cat_id'=>$item->id])->get();
                // dump($sub_id);
                foreach($sub_id as $sub_item) : ?>
                    <?php echo  $sub_item->content; ?><br>
                   
                <?php endforeach; ?>
            </div>
            <hr>
        <?php endforeach; ?>

</div>
</section><!--//section-->
@endsection
