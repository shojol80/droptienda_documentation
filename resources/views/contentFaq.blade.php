@extends('template/main')


@section('body')

<section class="docs-section ml-4 mt-5" id="item-1-1">
<h2 class="section-heading mb-4">Frequently asked questions</h2><hr><br>
<div class="container">
    <ul>
    <?php
        $data = DB::table('faq_infos')->get();
        foreach($data as $item) : ?>
            <li><h4>{{ $item->question }}</h4></li>
            <p>{{ $item->answer }}</p><br>
        <?php endforeach; ?>
    </ul>
</div>

</section><!--//section-->
@endsection
